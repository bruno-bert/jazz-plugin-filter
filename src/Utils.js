const getFileExtension = fileName => fileName.substr(fileName.lastIndexOf('.') + 1);

module.exports = {
  getFileExtension,
};

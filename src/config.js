export const name = "jazz-plugin-filter";
export const pipeline = [
  {
    id: "filterData",
    description:
      "This task is responsable for filtering the inputData based in a set of criterias and provide filtered data as output. It will also return the data that did not matched in the criteria, in a separated dataset.",
    class: "FilterDataTask"
  }
];

/*
export const inputParameters = {
  inputParm1: {
    attr1: "attr 1 - value 1",
    attr2: "attr 2 - value 2",    
  }
};*/

const source = "this";
const defaultRelativePluginPath = "../..";
const excelExtractorPluginPath = `${defaultRelativePluginPath}/jazz-plugin-excelextractor/dist`;
const FromPipeline = require("../FromPipeline");

module.exports = {
  pipeline: [
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:extract`,
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:validate`,
    `${source}:jazz-plugin-filter:filterData`
  ],
  plugins: [
    {
      name: "jazz-plugin-excelextractor",
      sheet: "source"
    },
    {
      name: "jazz-plugin-filter",
      tasks: {
        filterData: {
          postExecute: () => {
            const data = FromPipeline.getResult("filterData");
            console.log(data);
          },

          rawDataFrom: "extract",
          criteria: [
            {
              columnId: "amount",
              operator: "greater",
              value: 2500
            },
            {
              id: "customer_account_number",
              operator: "equal",
              value: 12345
            }
          ]
        }
      }
    }
  ]
};

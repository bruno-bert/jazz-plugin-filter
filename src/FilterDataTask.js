/* eslint-disable class-methods-use-this */
import Task from "./Task";
import { name as pluginName } from "./config";
import { filterData } from "./helpers";

class FilterDataTask extends Task {
  constructor(id, params, config, description = "", rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);

    [this.plugin] = this.config.plugins.filter(
      plugin => plugin.name === pluginName
    );

    this.validateParameters({
      ...params,
      criteria: this.plugin.tasks[id].criteria
    });
  }

  execute(data) {
    this.rawData = data || this.getRawData();
    const criteria = this.plugin.tasks[this.id].criteria;

    if (!this.validateConditionsForExecution()) return;

    return new Promise((resolve, reject) => {
      const result = filterData(this.rawData, criteria);
      resolve(result);
    });
  }

  onSuccess(info) {
    super.onSuccess(info);

    this.logger.success(
      `${String(this.constructor.name)} - Data filtered successfully`
    );
  }

  validateParameters(params) {
    if (!params.criteria) {
      this.onError("Filter Criteria is required");
    }
  }
}
module.exports = FilterDataTask;

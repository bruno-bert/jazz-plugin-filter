# Jazz Plugin: Filter Data Documentation

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.

## Plugin Pipeline

### Tasks

Contains a single task, named _filterData_.

#### filterData

**Purpose**: _filterData_ task is responsable for filtering the inputData based in a set of criterias and provide filtered data as output.
it will also return the data that did not matched in the criteria in a separated dataset.

**Class**: FilterDataTask

**Parameters**:

- criteria: an array of criterias used to filter the data

```javascript
criteria: [
  {
    columnId: "column_name_1",
    operator: "equal"
    value: "10"
  },
  {
    id: "column_name_2",
    operator: "not_equal"
    value: "null"
  }
];
```

Example:

With this data input:

```javascript
"data": [
    {
      "amount": 2000,
      "customer_account_number": 12345,
      "account": 2233112399,
      "transaction_date": "010119",
      "bankname": "BANCO DE GUATEMALA",
      "employee_tax_id": "5435-464.56",
      "employee_id": "00000000000000000001",
      "employee_name": "John Rodriguez",
      "employee_address": "calle prueba 1",
      "update_type": 2
    },

    {
      "amount": 3000,
      "customer_account_number": 12345,
      "account": 2232999999,
      "transaction_date": "010119",
      "bankname": "BANCO DE GUATEMALA",
      "employee_tax_id": 1111,
      "employee_id": "00000000000000000002",
      "employee_name": "Maria José",
      "employee_address": "calle prueba 2",
      "update_type": 2
    }
]
```

And this configuration (in pack-config.js file):

```javascript
criteria: [
  {
    columnId: "amount",
    operator: "greater"
    value: "2500"
  }
];
```

This will be the data output:

```javascript
[
  {
    amount: 3000,
    customer_account_number: 12345,
    account: 2232999999,
    transaction_date: "010119",
    bankname: "BANCO DE GUATEMALA",
    employee_tax_id: 1111,
    employee_id: "00000000000000000002",
    employee_name: "Maria José",
    employee_address: "calle prueba 2",
    update_type: 2
  }
];
```

### Pipeline

```javascript
export const pipeline = [
  {
    id: "filterData",
    description:
      "This task is responsable for filtering the inputData based in a set of criterias\n
      and provide filtered data as output. Also it will return the data that did not matched in the criteria"
    class: "FilterDataTask"
  }
];
```

### Input Parameters

_There are no input parameters to this plugin_
